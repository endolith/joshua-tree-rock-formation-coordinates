# Joshua Tree rock formation coordinates

Converted from the lists at:

- JoshuaTreeVillage.com: [Is this rock in the Wilderness?](https://www.joshuatreevillage.com:8000/515/gpsab.htm)
- [Joshua Tree National Park Wilderness Rock Climbing Study](https://npgallery.nps.gov/GetAsset/3b9bab14-51cb-43e2-b078-917d2be25d84) (Appendix IV)

These both seem to be subsets of some NPS master list that isn't otherwise available online.  NPS.gov responded:

> Hi, sorry to take so long to get back to you.  Your email got kicked around to about five people trying to find the list of wilderness and non-wilderness named rocks in JOTR that used to be available on our website.  Long story short, they are no longer on our website and cant seem to be tracked down.

[V-Z](https://www.joshuatreevillage.com:8000/515/gpsvz.htm) mentions a file called "Attributes of Rocks.shp" but that's the only other clue I can find.

[Coordinates were converted from NAD27 / UTM zone 11N (Easting/Northing) to WGS 84 (latitude/longitude)](https://gis.stackexchange.com/a/350694/7861) using the [`cs2cs` command](https://proj.org/apps/cs2cs.html):

    cs2cs +init=epsg:26711 +to +init=epsg:4326 -f "%.17f" coordinates.txt

This (like all tables of factual numerical information) is in the public domain.